# VatGEC

## Description

vatGEC is an Electronic Content Manager (GEC - Gestor Eletrónico de Conteúdos) designed to simplify and increase the management of air traffic. With vatGEC, controllers can easily see, organize, and access the most relevant information about their and other airports, as well as quickly display charts and weather information, all in a centralised location.

## Visuals & Features

#### MeteoHeader - Displayed airport related information, runway in use, QNH.

![meteoHeader](https://i.imgur.com/Dczvh3v.png)

#### Control Panel - Allows to control all information displayed.

![Control Menu](https://imgur.com/Om3lxhc.png)

#### TMA Display - display the TMA, STARTs and other information

![TMA Display](https://imgur.com/AM8kHjR.png)

#### Active areas and controllers - shows the active areas at the present time and displays active controllers on LPPC on VATSIM.

![Active areas & controllers](https://imgur.com/m4znTRB.png)

## Usage

vatGEC's software is hosted on [gec.vatsim.pt](https://gec.vatsim.pt) and should only be accessed by that URL.

A manual with detailed informations on how to use vatGEC's features is currently being developed.

## Support

All related support questions should be directed to any Portugal Vacc Tech Staff member or to Portugal Vacc's Web Master.

## Roadmap

vatGEC's current roadmap includes:

- Improvement of the active rwy logic
- QNH timer on change (when QNH changes, the yellow box only stays active for a certain duration)
- Introduce sub-menus and other pages
- Include more TMAs and other active areas
- Display METARs, TAFs, ATIS and Winds Aloft
- Display RFC procedures and other emergengy procedures and related information

## Contributing

Anyone can and is encourage to contribute to vatGEC.

We love suggestions and feedback! If you have them, please send us, so we can improve vatGEC!

If you have real images or knoledge about this software, if you can you are more that welcome to share them with us! We want to keep this as real as it can get.

If you want to be a part of our development team, please speak with Portugal Vacc's Tech Staff.

## Authors and acknowledgment

Developed by Portugal Vacc!

## Disclaimer: For Simulation Only

Our software has been developed for Portugal Vacc's controllers, and should only be used on a simulation environment, within VATSIM's network.

Any similarity with the software developed by Thales Group or NAV Portugal is not just a conincidence but in any situation this should be seen as plagiarims.

We are not afiliated with any of the companies mentioned before, and all our work is depeloped and mantained only by Portugal Vacc.
