import { Component } from '@angular/core';
import { ApiDataService } from './api-data.service';
import {
  airport_atis,
  airport_data,
  active_areas_info,
  controller,
  identification,
  present_weather,
  surface_wind,
  temperatures,
  visibility,
} from './models';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css'],
})
export class AppComponent {
  title = 'vatGEC';
  airportList = ['LPPT', 'LPMT', 'LPPR', 'LPFR', 'LPCS', 'LPMA'];

  airportData: airport_data[] = [];
  activeControllers: controller[] = [
    {
      cid: 0,
      name: '',
      callsign: '.',
      frequency: '',
      facility: 0,
      rating: 0,
      server: '',
      visual_range: 0,
      text_atis: [],
      last_updated: '',
      logon_time: '',
    },
  ];

  activeAreas: active_areas_info = {
    notice_info: {
      type: '',
      valid_wef: '',
      valid_til: '',
      released_on: '',
    },
    areas: [],
  };

  timerAPI = 60000;
  timerAreas = 60000 * 30;

  constructor(private apiData: ApiDataService) {}

  ngOnInit() {
    this.airportList.forEach((airport) => {
      const obj: airport_data = {
        icao: airport,
        metar: {
          identification: {} as identification,
          surface_wind: {} as surface_wind,
          visibility: {} as visibility,
          runway_visual_range: null,
          present_weather: {} as present_weather,
          clouds: [],
          cavok: false,
          temperatures: {} as temperatures,
          pressure: '--------',
          remarks: 'null',
          raw: '',
        },
        runways: [],
        low_visibility_ops: false,
        optimal_runways: ['----'],
        wind_components: [],
        airport_atis: {} as airport_atis,
      };
      this.airportData.push(obj);
    });

    this.getAirportData(this.airportList);
    this.getATIS();
    this.getActiveControllers();
    this.getActiveAreas();
  }

  getAirportData(airportList: string[]) {
    this.apiData.getAirportInfoParsed(airportList).subscribe((res) => {
      res.forEach((airport) => {
        this.airportData.every((icao) => {
          if (airport.icao === icao.icao) {
            if (airport.metar.identification.outdated) {
              return res.every((metarOutdated) => {
                if (
                  airport.metar.identification.refer_to_location ===
                  metarOutdated.icao
                ) {
                  icao.metar = metarOutdated.metar;
                  icao.runways = metarOutdated.runways;
                  icao.low_visibility_ops = metarOutdated.low_visibility_ops;
                  icao.optimal_runways = airport.optimal_runways;
                  icao.wind_components = metarOutdated.wind_components;
                  return false;
                }
                return true;
              });
            }
            icao.metar = airport.metar;
            icao.runways = airport.runways;
            icao.low_visibility_ops = airport.low_visibility_ops;
            icao.optimal_runways = airport.optimal_runways;
            icao.wind_components = airport.wind_components;
            return false;
          }
          return true;
        });
      });
    });
    console.log(this.airportData);
    setTimeout(() => this.getAirportData(airportList), this.timerAPI);
  }

  getATIS() {
    this.apiData.getATIS().subscribe((res) => {
      this.airportData.forEach((icao) => {
        if (
          res.every((atis) => {
            if (atis.callsign.includes(icao.icao) && atis.full_atis.length) {
              icao.airport_atis = atis;
              return false;
            }
            return true;
          })
        ) {
          icao.airport_atis = {} as airport_atis;
        }
      });
    });
    setTimeout(() => this.getATIS(), this.timerAPI);
  }

  getActiveControllers() {
    this.apiData.getControllers().subscribe((res) => {
      this.activeControllers = res;
    });
    setTimeout(() => this.getActiveControllers(), this.timerAPI);
  }

  getActiveAreas() {
    this.apiData.getActiveAreas().subscribe((res) => {
      this.activeAreas = res;
    });
    setTimeout(() => this.getActiveAreas(), this.timerAreas);
  }
}
