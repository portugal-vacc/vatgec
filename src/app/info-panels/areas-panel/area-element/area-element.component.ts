import { Component, Input } from '@angular/core';
import { area_display } from 'src/app/models';

@Component({
  selector: 'app-area-element',
  templateUrl: './area-element.component.html',
  styleUrls: ['./area-element.component.css'],
})
export class AreaElementComponent {
  @Input() areaInfo!: area_display;
}
