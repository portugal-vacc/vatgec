import { Component, Input } from '@angular/core';
import { area_display, active_areas_info } from 'src/app/models';

@Component({
  selector: 'app-areas-panel',
  templateUrl: './areas-panel.component.html',
  styleUrls: ['./areas-panel.component.css'],
})
export class AreasPanelComponent {
  currentTimeSeconds!: number;

  constructor() {
    this.currentTimeSeconds = this.checkActiveHours(
      new Date().toISOString().replace('Z', '')
    );
    setInterval(() => {
      this.currentTimeSeconds = this.checkActiveHours(
        new Date().toISOString().replace('Z', '')
      );
    }, 1000);
  }

  @Input() activeAreas: active_areas_info = {} as active_areas_info;
  areasDisplayed: area_display[] = [
    {
      name: 'TRA54',
      safeLevel: '-',
      active: false,
    },
    {
      name: 'TRA55',
      safeLevel: '-',
      active: false,
    },
    {
      name: 'R51BN',
      safeLevel: '-',
      active: false,
    },
    {
      name: 'R51BS',
      safeLevel: '-',
      active: false,
    },
    {
      name: 'R60B',
      safeLevel: '-',
      active: false,
    },
  ];

  ngOnInit() {
    this.verifyActiveAreas(this.activeAreas, this.areasDisplayed);
    const initialInterval = setInterval(() => {
      this.verifyActiveAreas(this.activeAreas, this.areasDisplayed);
      if (this.activeAreas.notice_info.type.length) {
        clearInterval(initialInterval);
      }
    }, 1000);
    setInterval(() => {
      this.verifyActiveAreas(this.activeAreas, this.areasDisplayed);
    }, 60000);
  }

  verifyActiveAreas(
    activeAreas: active_areas_info,
    areasDisplayed: area_display[]
  ) {
    areasDisplayed.forEach((area) => {
      if (
        activeAreas.areas.every((activeArea) => {
          if (
            activeArea.name.includes(area.name) &&
            this.checkActiveHours(activeArea.start_datetime) <
              this.currentTimeSeconds &&
            this.currentTimeSeconds <
              this.checkActiveHours(activeArea.end_datetime)
          ) {
            area.active = true;
            return false;
          }
          return true;
        })
      ) {
        area.active = false;
      }
    });
  }

  checkActiveHours(time: string) {
    const timeSplit = time.split('T')[1].split('+')[0].split(':');
    return +timeSplit[0] * 60 * 60 + +timeSplit[1] * 60 + +timeSplit[2];
  }
}
