import { ComponentFixture, TestBed } from '@angular/core/testing';

import { AreasPanelComponent } from './areas-panel.component';

describe('AreasPanelComponent', () => {
  let component: AreasPanelComponent;
  let fixture: ComponentFixture<AreasPanelComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ AreasPanelComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(AreasPanelComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
