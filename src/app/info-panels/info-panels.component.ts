import { Component, Input } from '@angular/core';
import { active_areas_info, controller } from '../models';

@Component({
  selector: 'app-info-panels',
  templateUrl: './info-panels.component.html',
  styleUrls: ['./info-panels.component.css'],
})
export class InfoPanelsComponent {
  @Input() activeControllers: controller[] = [];
  @Input() activeAreas!: active_areas_info;
}
