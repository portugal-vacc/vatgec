import { ComponentFixture, TestBed } from '@angular/core/testing';

import { InfoPanelsComponent } from './info-panels.component';

describe('InfoPanelsComponent', () => {
  let component: InfoPanelsComponent;
  let fixture: ComponentFixture<InfoPanelsComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ InfoPanelsComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(InfoPanelsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
