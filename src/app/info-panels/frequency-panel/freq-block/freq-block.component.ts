import { Component, Input } from '@angular/core';
import { frequency_display } from '../../../models';
@Component({
  selector: 'app-freq-block',
  templateUrl: './freq-block.component.html',
  styleUrls: ['./freq-block.component.css'],
})
export class FreqBlockComponent {
  @Input() frequency!: frequency_display;
}
