import { ComponentFixture, TestBed } from '@angular/core/testing';

import { FreqBlockComponent } from './freq-block.component';

describe('FreqBlockComponent', () => {
  let component: FreqBlockComponent;
  let fixture: ComponentFixture<FreqBlockComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ FreqBlockComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(FreqBlockComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
