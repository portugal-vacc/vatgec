import { ComponentFixture, TestBed } from '@angular/core/testing';

import { FrequencyPanelComponent } from './frequency-panel.component';

describe('FrequencyPanelComponent', () => {
  let component: FrequencyPanelComponent;
  let fixture: ComponentFixture<FrequencyPanelComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ FrequencyPanelComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(FrequencyPanelComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
