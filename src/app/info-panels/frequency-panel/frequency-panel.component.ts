import {
  Component,
  Input,
  KeyValueDiffers,
  KeyValueDiffer,
} from '@angular/core';
import { controller, frequency_display } from '../../models';

@Component({
  selector: 'app-frequency-panel',
  templateUrl: './frequency-panel.component.html',
  styleUrls: ['./frequency-panel.component.css'],
})
export class FrequencyPanelComponent {
  constructor(private differs: KeyValueDiffers) {}

  @Input() activeControllers: controller[] = [];
  controlerDiffer!: KeyValueDiffer<Array<controller>, any>;

  freqBlocks: frequency_display[][] = [
    [
      {
        name: 'TMA LI',
        params: ['LPPT', 'U', 'APP'],
        freq: '123.980',
        active: 'false',
      },
      {
        name: 'OESL',
        params: ['LPPC', 'W', 'CTR'],
        freq: '131.325',
        active: 'false',
      },
      {
        name: 'NORL',
        params: ['LPPC', 'N', 'CTR'],
        freq: '132.305',
        active: 'false',
      },
      {
        name: 'MADL',
        params: ['LPPC', 'I', 'CTR'],
        freq: '132.255',
        active: 'false',
      },
      { name: 'LECM', params: [''], freq: '133.755', active: 'blue' },
    ],
    [
      {
        name: 'APP LI',
        params: ['LPPT', 'APP'],
        freq: '119.105',
        active: 'false',
      },
      { name: 'C', params: ['LPPC', 'CTR'], freq: '125.550', active: 'false' },
      {
        name: 'CENL',
        params: ['LPPC', 'C', 'CTR'],
        freq: '136.030',
        active: 'false',
      },
      {
        name: 'FIS',
        params: ['LPAM', 'CTR'],
        freq: '123.755',
        active: 'false',
      },
      { name: 'LECS', params: [''], freq: '133.350', active: 'blue' },
    ],
    [
      {
        name: 'ARR LI',
        params: ['LPPT', ' F', 'APP'],
        freq: '125.130',
        active: 'false',
      },
      {
        name: 'ESTL',
        params: ['LPPC', 'E', 'CTR'],
        freq: '132.850',
        active: 'false',
      },
      {
        name: 'SULL',
        params: ['LPPC', 'S', 'CTR'],
        freq: '132.705',
        active: 'false',
      },
      {
        name: 'NORU',
        params: ['LPPC', 'NU', 'CTR'],
        freq: '127.255',
        active: 'false',
      },
      {
        name: 'SEV APP',
        params: [''],
        freq: '120.800',
        active: 'blue',
      },
    ],
    [
      {
        name: 'TMA PR',
        params: ['LPPR', 'APP'],
        freq: '120.910',
        active: 'blue',
      },
      {
        name: 'TMA FR',
        params: ['LPFR', 'APP'],
        freq: '119.405',
        active: 'blue',
      },
      {
        name: 'TMA MA',
        params: ['LPMA', 'APP'],
        freq: '119.605',
        active: 'blue',
      },
      { name: 'BEJA', params: ['LPBJ'], freq: '130.100', active: 'blue' },
      {
        name: 'STO APP',
        params: [''],
        freq: '120.200',
        active: 'blue',
      },
    ],
  ];

  ngOnInit() {
    this.controlerDiffer = this.differs.find(this.activeControllers).create();
  }

  ngDoCheck() {
    const changes = this.controlerDiffer.diff(this.activeControllers);
    if (changes) {
      this.freqBlocks.forEach((freqCol) => {
        freqCol.forEach((freq) => {
          if (freq.active === 'blue') {
            return;
          }
          if (
            this.activeControllers.every((activeController) => {
              const sep = activeController.callsign.replace(
                /_T_|_M_|_|\d/g,
                ''
              );
              const params = JSON.stringify(freq.params).replace(
                /["[\],]/g,
                ''
              );
              if (sep === params) {
                freq.active = 'true';
                return false;
              }
              return true;
            })
          ) {
            freq.active = 'false';
          }
        });
      });
    }
  }
}
