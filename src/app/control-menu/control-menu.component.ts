import { Component } from '@angular/core';

@Component({
  selector: 'app-control-menu',
  templateUrl: './control-menu.component.html',
  styleUrls: ['./control-menu.component.css'],
})
export class ControlMenuComponent {
  date = '--:--:--';

  constructor() {
    this.date = Date.now().toString();
    setInterval(() => {
      this.date = Date.now().toString();
    }, 1000);
  }
}
