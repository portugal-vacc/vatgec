export interface identification {
  location: string;
  issued: string;
  outdated: boolean;
  refer_to_location: string;
}

export interface surface_wind {
  wind_direction: number;
  variable: boolean;
  wind_speed: number;
  gust: boolean;
  maximum_wind_speed: number;
  unit: string;
  wind_variable_from: number;
  variability: boolean;
  wind_variable_to: number;
}

export interface present_weather {
  intensity: string;
  descriptor: string;
  precipoitation: string;
  obscuration: string;
  other: string;
}

export interface cloud {
  cloud_amount: string;
  height_of_base: string;
  cloud_type: string;
}

export interface visibility {
  prevailing_visibility: number;
  lowest_visibility: null;
  lowest_visibility_direction: null;
}

export interface temperatures {
  air_temperature: number;
  dew_point_temperature: number;
}

export interface airport_metar {
  identification: identification;
  surface_wind: surface_wind;
  visibility: visibility;
  runway_visual_range: null;
  present_weather: present_weather;
  clouds: cloud[];
  cavok: boolean;
  temperatures: temperatures;
  pressure: string;
  remarks: string;
  raw: string;
}

export interface airport_atis {
  cid: number;
  name: string;
  callsign: string;
  frequency: string;
  rating: number;
  server: string;
  visual_range: number;
  atis_code: string;
  text_atis: string[];
  last_updated: string;
  logon_time: string;
  full_atis: string;
  lvp: boolean;
  runway_active: string;
}

export interface wind_components {
  runway: string;
  wind_speed: number;
  wind_direction: number;
  angle_difference: number;
  cross_wind: number;
  head_wind: number;
}
export interface airport_data {
  icao: string;
  metar: airport_metar;
  runways: string[];
  low_visibility_ops: boolean;
  optimal_runways: string[];
  wind_components: wind_components[];
  airport_atis: airport_atis;
}

export interface controller {
  cid: number;
  name: string;
  callsign: string;
  frequency: string;
  facility: number;
  rating: number;
  server: string;
  visual_range: number;
  text_atis: string[];
  last_updated: string;
  logon_time: string;
}

export interface frequency_display {
  name: string;
  params: string[];
  freq: string;
  active: string;
}

export interface area_info {
  name: string;
  minimum_fl: number;
  maximum_fl: number;
  start_datetime: string;
  end_datetime: string;
}
export interface active_areas_info {
  notice_info: {
    type: string;
    valid_wef: string;
    valid_til: string;
    released_on: string;
  };
  areas: area_info[];
}

export interface area_display {
  name: string;
  safeLevel: string;
  active: boolean;
}
