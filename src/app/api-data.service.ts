import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

import {
  airport_atis,
  airport_data,
  active_areas_info,
  controller,
} from './models';

@Injectable({
  providedIn: 'root',
})
export class ApiDataService {
  readonly ROOT_METEO_URL = 'https://meteo.vatsim.pt/api';

  constructor(private http: HttpClient) {}

  concatenateAirports(airportList: string[]) {
    let list = '';
    let i = 0;
    airportList.forEach((airport) => {
      if (i + 1 === airportList.length) {
        list += airport;
      } else {
        list += airport + ',';
        i++;
      }
    });
    return list;
  }

  getAirportInfoParsed(airportList: string[]) {
    return this.http.get<airport_data[]>(
      'https://meteo.vatsim.pt/api/v1/airport?airport_icao_codes=' +
        this.concatenateAirports(airportList)
    );
  }

  getATIS() {
    return this.http.get<airport_atis[]>(
      'https://vatdata.vatsim.pt/api/v1/live/atis/lp'
    );
  }

  getControllers() {
    return this.http.get<controller[]>(
      'https://vatdata.vatsim.pt/api/v1/live/controllers/LP'
    );
  }

  getActiveAreas() {
    return this.http.get<active_areas_info>(
      'https://lara.vatsim.pt/api/areas/LP'
    );
  }
}
