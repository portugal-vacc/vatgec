import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { HttpClientModule } from '@angular/common/http';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { MeteoHeaderComponent } from './meteo-header/meteo-header.component';
import { MeteoElementComponent } from './meteo-header/meteo-element/meteo-element.component';
import { LpptTmaDisplayComponent } from './airport-main-content/lppt-tma-display/lppt-tma-display.component';
import { AirportMainContentComponent } from './airport-main-content/airport-main-content.component';
import { ControlMenuComponent } from './control-menu/control-menu.component';
import { FrequencyPanelComponent } from './info-panels/frequency-panel/frequency-panel.component';
import { FreqBlockComponent } from './info-panels/frequency-panel/freq-block/freq-block.component';
import { InfoPanelsComponent } from './info-panels/info-panels.component';
import { AreasPanelComponent } from './info-panels/areas-panel/areas-panel.component';
import { AreaElementComponent } from './info-panels/areas-panel/area-element/area-element.component';

@NgModule({
  declarations: [
    AppComponent,
    MeteoHeaderComponent,
    MeteoElementComponent,
    LpptTmaDisplayComponent,
    AirportMainContentComponent,
    ControlMenuComponent,
    FrequencyPanelComponent,
    FreqBlockComponent,
    InfoPanelsComponent,
    AreasPanelComponent,
    AreaElementComponent,
  ],
  imports: [BrowserModule, AppRoutingModule, HttpClientModule],
  providers: [],
  bootstrap: [AppComponent],
})
export class AppModule {}
