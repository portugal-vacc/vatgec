import {
  Component,
  Input,
  KeyValueDiffers,
  KeyValueDiffer,
} from '@angular/core';
import { airport_data } from 'src/app/models';

@Component({
  selector: 'app-lppt-tma-display',
  templateUrl: './lppt-tma-display.component.html',
  styleUrls: ['./lppt-tma-display.component.css'],
})
export class LpptTmaDisplayComponent {
  constructor(private _differsService: KeyValueDiffers) {}

  activeRunway = '';

  @Input() airportTMA!: airport_data;

  airportInfoDiffer!: KeyValueDiffer<string, any>;

  ngOnInit() {
    this.airportInfoDiffer = this._differsService
      .find(this.airportTMA)
      .create();
  }

  ngDoCheck() {
    if (this.airportInfoDiffer) {
      const changes = this.airportInfoDiffer.diff(this.airportTMA);

      if (changes) {
        if (this.airportTMA.airport_atis?.runway_active) {
          this.activeRunway = this.airportTMA.airport_atis?.runway_active;
        } else {
          if (this.airportTMA.optimal_runways.length > 1) {
            this.activeRunway = '02';
          } else {
            this.activeRunway = this.airportTMA.optimal_runways[0];
          }
        }
      }
    }
  }
}
