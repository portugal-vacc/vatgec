import { ComponentFixture, TestBed } from '@angular/core/testing';

import { LpptTmaDisplayComponent } from './lppt-tma-display.component';

describe('LpptTmaDisplayComponent', () => {
  let component: LpptTmaDisplayComponent;
  let fixture: ComponentFixture<LpptTmaDisplayComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [LpptTmaDisplayComponent],
    }).compileComponents();

    fixture = TestBed.createComponent(LpptTmaDisplayComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
