import { ComponentFixture, TestBed } from '@angular/core/testing';

import { AirportMainContentComponent } from './airport-main-content.component';

describe('AirportMainContentComponent', () => {
  let component: AirportMainContentComponent;
  let fixture: ComponentFixture<AirportMainContentComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ AirportMainContentComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(AirportMainContentComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
