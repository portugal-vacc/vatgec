import {
  Component,
  DoCheck,
  Input,
  KeyValueDiffers,
  KeyValueDiffer,
} from '@angular/core';
import { airport_data } from '../models';

@Component({
  selector: 'app-airport-main-content',
  templateUrl: './airport-main-content.component.html',
  styleUrls: ['./airport-main-content.component.css'],
})
export class AirportMainContentComponent implements DoCheck {
  constructor(private differs: KeyValueDiffers) {}

  @Input() airportData!: airport_data[];
  selectedTMA = 'LPPT';
  TMAObject!: airport_data;
  AirportDiffer!: KeyValueDiffer<Array<airport_data>, any>;

  // Possible code refactor to inject the airport directly without using DoCheck

  ngOnInit() {
    this.AirportDiffer = this.differs.find(this.airportData).create();
  }

  ngDoCheck() {
    const changes = this.AirportDiffer.diff(this.airportData);
    if (changes) {
      this.airportData.every((airport) => {
        if (airport.icao === this.selectedTMA) {
          this.TMAObject = airport;
          return false;
        }
        return true;
      });
    }
  }
}
