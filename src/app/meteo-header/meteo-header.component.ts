import { Component, Input } from '@angular/core';
import { airport_data } from '../models';

@Component({
  selector: 'app-meteo-header',
  templateUrl: './meteo-header.component.html',
  styleUrls: ['./meteo-header.component.css'],
})
export class MeteoHeaderComponent {
  @Input() airportData!: airport_data[];
}
