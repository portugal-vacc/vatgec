import {
  Component,
  DoCheck,
  Input,
  KeyValueDiffers,
  KeyValueDiffer,
} from '@angular/core';
import { airport_data } from '../../models';

@Component({
  selector: 'app-meteo-element',
  templateUrl: './meteo-element.component.html',
  styleUrls: ['./meteo-element.component.css'],
})
export class MeteoElementComponent implements DoCheck {
  constructor(private differs: KeyValueDiffers) {}

  @Input() airportData!: airport_data;

  newQNH = false;
  currentQNH = '';
  lvoOps = false;
  airportDataDiffer!: KeyValueDiffer<string, any>;

  ngOnInit() {
    this.airportDataDiffer = this.differs.find(this.airportData).create();
  }

  ngDoCheck() {
    if (this.airportDataDiffer) {
      const changes = this.airportDataDiffer.diff(this.airportData);
      if (changes) {
        if (
          this.currentQNH !== '' &&
          this.currentQNH !== '--------' &&
          this.airportData.metar.pressure !== this.currentQNH
        ) {
          this.newQNH = true;
        }
        this.currentQNH = this.airportData.metar.pressure;

        if (Object.keys(this.airportData.airport_atis).length) {
          this.lvoOps = this.airportData.airport_atis.lvp;
        } else {
          this.lvoOps = this.airportData.low_visibility_ops;
        }
      }
    }
  }

  resetQNH() {
    this.newQNH = false;
  }
}
