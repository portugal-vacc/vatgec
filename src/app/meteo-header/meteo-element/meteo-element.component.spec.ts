import { ComponentFixture, TestBed } from '@angular/core/testing';

import { MeteoElementComponent } from './meteo-element.component';

describe('MeteoElementComponent', () => {
  let component: MeteoElementComponent;
  let fixture: ComponentFixture<MeteoElementComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ MeteoElementComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(MeteoElementComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
